from mads_alarm_states import Alarm_State

import tkinter as tk
from tkinter import font
import pyaudio
import wave
import threading
import time
import os
import subprocess

class MADSWindow(tk.Tk):

    def __init__(self, *args, window_title='MADS', window_dimensions='1000x600', background='white', alert_control=None,
                 title_bar_custom = False, ** kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        # Alert control
        self.alert_control = alert_control

        # Values for changing Gui appearance
        self.animation_count = 0

        # Alarm audio
        self.alarm_audio_file_name = "mads_alarm_sound.wav"
        self.audio_is_playing = False

        # Alarm state and vars
        self.alarm_state = Alarm_State.WAITING
        self.alarm_standby_time_in_seconds = 180
        self.alarm_standby_timer = None
        self.alarm_start_time = None

        self.title(window_title)
        self.geometry(window_dimensions)
        self.configure(bg=background)
        self.resizable(0, 0)

        # Fonts
        self.window_font = font.Font(family='Arial', underline=1, size=16, weight='bold')
        self.button_font = font.Font(family='Arial', size=16, weight='bold')
        self.scanning_font = font.Font(family='Arial', size=24, weight='bold')

        # Having some trouble subclassing the label and accessing the text attribute idk this work though
        self.title_mads = tk.Label(self, text="Microphone Array Detection System")
        self.title_mads.config(bg='white', font=self.window_font)
        self.title_mads.place(relx=0.5, rely=0.05, anchor=tk.CENTER)

        # Canvas for detection state
        self.hexagon_state_canvas = tk.Canvas(self, width=500, height=500)
        self.hexagon_state_canvas.config(borderwidth=0,
                                         highlightbackground='white',
                                         bg='white')
        self.id_hex = self.hexagon_state_canvas.create_polygon([250,5,500,150,500,350,250,500,5,350,5,150],
                                                               outline='black',
                                                               fill='#00C947',
                                                               width=2)
        self.hexagon_state_canvas.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

        # Canvas for hexagon informing the user the system is searching and the detection status
        self.searching_for_drone_canvas = tk.Canvas(self, width=200, height=100)
        self.searching_for_drone_canvas.config(borderwidth=0, highlightbackground='#00C947', bg='#00C947')
        self.id_text_search = self.searching_for_drone_canvas.create_text((100,50),
                                                                          text='Scanning',
                                                                          disabledfill='black',
                                                                          font=self.scanning_font)
        self.searching_for_drone_canvas.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

        # Log browsing button
        self.disable_alarm_button = tk.Button(text='Disable Alarm',
                                           font=self.button_font,
                                           bg='#FFB300',
                                           command=self.disable_alarm).place(relx=.1, rely=.9)

        self.enable_alarm_button = tk.Button(text='Enable Alarm',
                                           font=self.button_font,
                                           bg='#FFB300',
                                           command=self.able_alarm).place(relx=.415, rely=.9)

        #self.enable_alarm_button = tk.Button(text='Sound Alarm',
        #                                   font=self.button_font,
        #                                   bg='#FFB300',
        #                                   command=self.blu_alarm).place(relx=.415, rely=.1)

        # Disable alarm button
        self.browse_log_button = tk.Button(text='Browse Log', font=self.button_font, bg='#FFB300', command=self.open_log).place(relx=.7, rely=.9)

    def disable_alarm(self):
        if self.alarm_state != Alarm_State.DISABLED and self.alert_control.value == 1:
            self.alert_control.value = 0
            self.alarm_state = Alarm_State.DISABLED
            self.alarm_start_time = time.time()
            self.alarm_standby_timer = threading.Timer(180, self.enable_alarm)
            self.alarm_standby_timer.start()

    def open_log(self):
        if os.name != "nt":
            subprocess.Popen(['gedit', 'mads_drone.log'])
        else:
            subprocess.Popen('Notepad mads_drone.log')

    def enable_alarm(self):
        self.alert_control.value = 0
        self.alarm_state = Alarm_State.WAITING
        self.alarm_standby_time_in_seconds = 180

    def able_alarm(self):
        self.alert_control.value = 0
        self.alarm_state = Alarm_State.WAITING

    def blu_alarm(self):
        self.alert_control.value = 1
        self.alarm_state = Alarm_State.ENABLE

    def animate_gui(self, terminate_process_sentinel):

        if self.alarm_state == Alarm_State.DISABLED:

            if self.alarm_standby_time_in_seconds >= 180:
                time_left = "03:00"
                self.alarm_standby_time_in_seconds = (180 - (int(time.time() - self.alarm_start_time)))
            elif self.alarm_standby_time_in_seconds > 120:
                self.alarm_standby_time_in_seconds = (180- (int(time.time() - self.alarm_start_time)))
                time_left = "02:" + (str(self.alarm_standby_time_in_seconds % 60)).zfill(2)
            elif self.alarm_standby_time_in_seconds > 60:
                self.alarm_standby_time_in_seconds = (180 - (int(time.time() - self.alarm_start_time)))
                time_left = "01:" + (str(self.alarm_standby_time_in_seconds % 60)).zfill(2)
            else:
                self.alarm_standby_time_in_seconds = (180 - (int(time.time() - self.alarm_start_time)))
                time_left = "00:" + (str(self.alarm_standby_time_in_seconds % 60)).zfill(2)

            self.change_color_on_state('#FFB300',
                                       '#FCC238',
                                       "   Alarm\nDisabled\n   "+time_left,
                                       "   Alarm\nDisabled\n   "+time_left,
                                       "   Alarm\nDisabled\n   "+time_left,
                                       "   Alarm\nDisabled\n   "+time_left)
        elif self.alert_control.value and self.alarm_state != Alarm_State.DISABLED:
            self.change_color_on_state('#FF0004', '#ED1C20', "   Drone\nDetected!", "", "   Drone\nDetected!", "")
            if not self.audio_is_playing:
                self.audio_is_playing = True
                self.alarm_state = Alarm_State.ENABLED
                audio_thread = threading.Thread(target=self.play_audio, args=(terminate_process_sentinel,))
                audio_thread.start()
        else:
            self.change_color_on_state('#08D14F', '#00C947', " Scanning.", "  Scanning..", "   Scanning...", "Scanning")

        self.after(1000, self.animate_gui, terminate_process_sentinel)

    def change_color_on_state(self, color1, color2, text_state1, text_state2, text_state3, text_state4):

        if self.animation_count == 0:
            self.searching_for_drone_canvas.config(bg=color1, highlightbackground=color1)
            self.hexagon_state_canvas.itemconfig(self.id_hex, fill=color1)
            self.searching_for_drone_canvas.itemconfig(self.id_text_search, text=text_state1)
            self.animation_count += 1
        elif self.animation_count == 1:
            self.hexagon_state_canvas.itemconfig(self.id_hex, fill=color2)
            self.searching_for_drone_canvas.config(bg=color2, highlightbackground=color2)
            self.searching_for_drone_canvas.itemconfig(self.id_text_search, text=text_state2)
            self.animation_count += 1
        elif self.animation_count == 2:
            self.searching_for_drone_canvas.config(bg=color1, highlightbackground=color1)
            self.hexagon_state_canvas.itemconfig(self.id_hex, fill=color1)
            self.searching_for_drone_canvas.itemconfig(self.id_text_search, text=text_state3)
            self.animation_count += 1
        else:
            self.searching_for_drone_canvas.config(bg=color2, highlightbackground=color2)
            self.hexagon_state_canvas.itemconfig(self.id_hex, fill=color2)
            self.searching_for_drone_canvas.itemconfig(self.id_text_search, text=text_state4)
            self.animation_count = 0

    def play_audio(self, terminate_process_sentinel):

        chunk = 1024
        wf = wave.open(self.alarm_audio_file_name, 'rb')
        p = pyaudio.PyAudio()
        stream = p.open(
            format=p.get_format_from_width(wf.getsampwidth()),
            channels=wf.getnchannels(),
            rate=wf.getframerate(),
            output=True)
        data = wf.readframes(chunk)
        while terminate_process_sentinel.value and self.alarm_state == Alarm_State.ENABLED:
            stream.write(data)
            data = wf.readframes(chunk)
            if data == b'':
                wf.rewind()
                data = wf.readframes(chunk)
        self.audio_is_playing = False
        stream.stop_stream()
        stream.close()
        p.terminate()
