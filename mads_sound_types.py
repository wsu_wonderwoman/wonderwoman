from enum import Enum

class SoundTypes(Enum):

    # Drones are reserved for 0-50
    GENERICDRONE = 0
    FPVRACINGQUAD250 = 1
    CX10 = 2
    DJIPHANT4 = 3
    HOLYSTONE = 4
    # Other sounds reserved for > 50
    TALKING = 51
    GROUPTALKING = 52
    TRAFFIC = 53
    BIRDS = 54
    MUSIC = 55
    RAIN = 56
    WEEDWACKER = 57

