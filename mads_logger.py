import datetime

class Logger:

    def __init__(self, outfile='mads_drone.log', clear_log=False):

        self.detection_count = 0
        self.date_time = None
        self.out_file = outfile
        self.clear_log = clear_log

        if self.clear_log:
            open('mads_drone.log', 'w').close()

# _______________________________________________________________________________________________________END of __init__

    def log_detection(self, signature_match):

        with open(self.out_file, mode='a', encoding='utf-8') as myFile:
            self.detection_count += 1
            self.date_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            myFile.write('{}: {}        Signature:{}\n'.format(self.detection_count, self.date_time, signature_match))
            myFile.close()

# _________________________________________________________________________________________END of function log_detection

# ___________________________________________________________________________________________________END of class Logger
