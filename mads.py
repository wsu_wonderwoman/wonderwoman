# Imports from MADS
from mads_mic import Microphone
from mads_rfft import MadsRFFT
from mads_feature_extraction import AudioFeatureExtraction
from mads_signature import MADSSignature
from mads_logger import Logger
from mads_gui import MADSWindow

# Standard library imports
from multiprocessing import Process
from multiprocessing import Value

##########################################################################
# Class: Senior Projects 2
# Team: E/WonderWomen
# Project: Microphone Array Detection System
# Completion Date: 2nd week of December 2017
# Authors: Mohammad Alabanni, Ivan Frasure, Troy Frommer, Matthew Williams
##########################################################################

##########################################################################
# Values for changing the generated signature form
# BIN_LOWER: Lowest frequency considered will bin anything lower than
#            this bound to the zeroth bin
# BIN_UPPER: Highest frequency considered will bin anything higher than
#            this bound to the ((BIN_UPPER - BIN_LOWER) / BIN_SIZE) bin
# BIN_SIZE: The size of the interval to 'carve' a bin into
# NUMBER_OF_PEAK_FREQUENCIES: The length of the signature (determined
#                             by frequencies with the highest amplitude)
##########################################################################
BIN_LOWER = 30
BIN_UPPER = 8000
BIN_SIZE = 25
NUMBER_OF_PEAK_FREQUENCIES = 10

##########################################################################
# Changes the run state of the program from Training to Detection
# DETECTION_MODE: State of detection for the system
# SIGNATURE_GENERATION_MODE: Create signatures and write to
#                            mads_training_data.csv (
#                            used for training the model)
# CLEAR_TRAINING_DATABASE: State as to if the database should be cleared.
#                          Clears mads_training_data.csv
#                          WARNING: Will lose all training data if True
##########################################################################
DETECTION_MODE = True
SIGNATURE_GENERATION_MODE = False
CLEAR_TRAINING_DATABASE = False


def create_essentials_for_mads_operation():
    ####################################################################################################################
    # This function sets up the objects that are essential to run the signature generation
    # or the detection functionality of the system.
    #
    # @param: N/A
    #
    # @return: Name-microphone Type-Microphone - The microphone that the system will use to receive audio signals.
    #                                            See mads_mic.py for implementation details.
    #
    # @return: Name-mads_rfft Type-MadsRFFT - The object that runs the real fast fourier transform on the signal that
    #                                         Microphone object is receiving. See mads_rfft.py for implementation
    #                                         details.
    #
    # @return: Name-signal_feature_extractor Type-AudioFeatureExtraction - This object extracts features from the audio
    #                                                                      signal post rfft.
    #                                                                      See mads_feature_extraction.py for
    #                                                                      implementation details.
    #                                                                      Methods: get_spectral_peaks,
    #                                                                               bin_spectral_peaks,
    #                                                                               and get_total_energy.
    #
    # @return Name-signature_checker Type-MADSSignature - This object is used to train the neural network model as well
    #                                                     as detect drones using the aforementioned neural network.
    #
    # Notes:
    ####################################################################################################################

    # Create the microphone and the real fast fourier transform object
    microphone = Microphone()
    mads_rfft = MadsRFFT(microphone.length, microphone.Hz)

    # These variables passed into these objects are defined as constants at the top of this file so they can be quickly
    # changed to adjust operation. Create the signal_feature_extractor and signature_checker objects.
    signal_feature_extractor = AudioFeatureExtraction(bin_lower=BIN_LOWER,
                                                      bin_upper=BIN_UPPER,
                                                      bin_size=BIN_SIZE,
                                                      number_of_peak_frequencies=NUMBER_OF_PEAK_FREQUENCIES,
                                                      mads_rfft=mads_rfft)
    signature_checker = MADSSignature(detection_mode=DETECTION_MODE,
                                      signature_generation_mode=SIGNATURE_GENERATION_MODE,
                                      clear_training_database=CLEAR_TRAINING_DATABASE)

    # Return all these objects to the run detection function
    return microphone, mads_rfft, signal_feature_extractor, signature_checker
# __________________________________________________________________END of function create_essentials_for_mads_operation


def setup_detection_mode(signature_checker=None, log_file_name=None):
    ####################################################################################################################
    # This function runs if the system is running in detection mode. It simply creates a logger object to output
    # detection events to and loads the detection model (the neural network).
    #
    # @param: Name-signature_checker Type-MADSSignature - The object that trains the data or classifies incoming audio
    #                                                     signals as drones or something else. Passed here to load the
    #                                                     machine learning model.
    #
    # @return: Name-log_file_name Type-String - The file that the system will write to if a detection event occurs.
    #
    # @return: Name-logger Type-Logger - The object that logs detection events. Methods: log_detection
    #
    # Notes:
    ####################################################################################################################
    logger = Logger(outfile=log_file_name, clear_log=False)
    signature_checker.load_detection_model()  # Load neural network model
    return logger
# __________________________________________________________________________________END of function setup_detection_mode


def signature_mode_logic(signature_checker, signal_feature_extractor, total_energy):
    ####################################################################################################################
    # This function generates signatures for the training data for the neural network model which is written to
    # mads_training_data.csv.
    #
    # @param: Name-signature_checker Type-MADSSignature - The object that trains the data or classifies incoming audio
    #                                                     signals as a drone or something else. Passed here to load the
    #                                                     machine learning model.
    #
    # @param: Name-signal_feature_extractor Type-AudioFeatureExtraction - The object that contains the spectral peaks
    #                                                                      and binning data.
    #
    # @return: N/A
    #
    # Notes:
    ####################################################################################################################
    signature_checker.generate_signatures_for_training_data(signal_feature_extractor, total_energy)
# __________________________________________________________________________________END of function signature_mode_logic


def detection_mode_logic(signature_checker, alert_gui_detection_sentinel, logger, signal_feature_extractor, total_energy):
    ####################################################################################################################
    # This function checks the current audio signal's signature and runs it through the neural network model in order to
    # classify it.
    #
    # @param: Name-signature_checker Type-MADSSignature - The object that trains the data or classifies incoming audio
    #                                                     signals as a drone or something else. Passed here to to alert
    #                                                     gui and logger if a detection event has occurred.
    #
    # @param Name-alert_gui_detection_sentinel Type-shared variable - The variable that alerts the gui process if a
    #                                                                 detection event has occurred.
    #
    # @param Name-logger Type-Logger - The logging object that logs detection events to the log file if a detection
    #                                  occurs.
    #
    # @param: Name-signal_feature_extractor Type-AudioFeatureExtraction - The object that contains the spectral peaks
    #                                                                     and binning data.
    #
    # @return: N/A
    #
    # Notes:
    ####################################################################################################################

    detection_state, signature_match = signature_checker.check_current_signature_against_model(signal_feature_extractor, total_energy)

    if detection_state:  # If a drone is detected
        alert_gui_detection_sentinel.value = 1
        logger.log_detection(signature_match)
# __________________________________________________________________________________END of function detection_mode_logic


def run_detection(terminate_process_sentinel, alert_gui_detection_sentinel):
    ####################################################################################################################
    # This function gets called repeatedly. This will run the get_spectrum_data function
    # which runs the rfft. The transform gets put into the variable spectrum.
    # Next the fingerprint is calculated (the spectral peaks from the spectrum).
    # Finally this function checks if the fingerprint is in the signature database
    # and if it is, it changes the shared variable with the gui to alert it of a detection
    # and then the logger logs the detection event.
    #
    # @param: Name-terminate_process_sentinel Type-shared variable - Used to stop the detection when the gui is closed.
    #
    # @param: Name-alert_gui_detection_sentinel Type-shared variable - Used to alert the gui that a detection has
    #                                                                  occurred.
    # @return: N/A
    #
    # Notes:
    ####################################################################################################################

    # Create a Microphone, MadsRFFT, MADSFingerPrinter, MADSSignature
    microphone, mads_rfft, signal_feature_extractor, signature_checker = create_essentials_for_mads_operation()

    # Load neural network model and create a Logger object
    if signature_checker.detection_mode:
        logger = setup_detection_mode(signature_checker, log_file_name='mads_drone.log')

    # Start mic thread
    microphone.start_thread()

    # While the gui is open
    while terminate_process_sentinel.value:

        # The power spectrum of the current signal
        spectrum = mads_rfft.run_rfft(microphone.data)

        # Get spectral peaks and bin them
        signal_feature_extractor.get_spectral_peaks(spectrum)
        signal_feature_extractor.bin_spectral_peaks()

        # Get the total energy
        total_energy = signal_feature_extractor.get_total_energy(spectrum)

        # Training the data
        if signature_checker.signature_generation_training_mode:
            signature_mode_logic(signature_checker, signal_feature_extractor, total_energy)

        # Detecting audio signals (drone signals)
        if signature_checker.detection_mode:
            detection_mode_logic(signature_checker,
                                 alert_gui_detection_sentinel,
                                 logger,
                                 signal_feature_extractor,
                                 total_energy)

    # Join the microphone thread
    microphone.finish = True

# ________________________________________________________________________________________________END of function update


def gui_run(terminate_process_sentinel, alert_gui_detection):
    ####################################################################################################################
    # This function runs the gui for the system.
    #
    # @param: Name-terminate_process_sentinel Type-shared variable - Used to stop the detection when the gui is closed.
    #
    # @param: Name-alert_gui_detection_sentinel Type-shared variable - Used to alert the gui that a detection has
    #                                                                  occurred.
    # @return: N/A
    #
    # Notes:
    ####################################################################################################################
    mads_gui = MADSWindow(alert_control=alert_gui_detection)
    mads_gui.after(1000, mads_gui.animate_gui(terminate_process_sentinel))
    mads_gui.mainloop()
    terminate_process_sentinel.value = 0
# ________________________________________________________________________________________________________END of gui_run


def start_process(*args, target_to_run=None):
    ####################################################################################################################
    # This function starts process.
    #
    # @param: Name-*args Type-undefined - Used to pass arguments to process.
    #
    # @param: Name-target_to_run Type-function - The target of the multiprocess.
    #
    # @return: Name-process Type-Process - The process this function started
    #
    # Notes:
    ####################################################################################################################
    process = Process(target=target_to_run, args=args)
    process.start()
    return process
# __________________________________________________________________________________________________END of start_process


def join_processes(*args):
    ####################################################################################################################
    # This function joins process.
    #
    # @param: Name-*args Type-Process - Used to join multiple processes.
    #
    # @return: N/A
    #
    # Notes:
    ####################################################################################################################
    # Join all the processes passed to this function
    for arg in args:
        arg.join()
# _________________________________________________________________________________________________END of join_processes


def main():
    ####################################################################################################################
    # This function is the entry point into the system. It setups up the shared variables for gui and the detection
    # processes. It also starts these processes and joins them.
    #
    # @param: N/A
    #
    # @return: N/A
    #
    # Notes:
    ####################################################################################################################
    # This is a shared memory variable that shuts down the detection process if the gui window closes
    terminate_process_sentinel = Value('i', 1)

    # This is a shared memory variable that alerts the gui if a detection has occurred by the detection process
    alert_gui_detection_sentinel = Value('i', 0)

    # Start the gui process
    gui_process = start_process(terminate_process_sentinel, alert_gui_detection_sentinel, target_to_run=gui_run)

    # Start the detection process
    detection_process = start_process(terminate_process_sentinel,
                                      alert_gui_detection_sentinel,
                                      target_to_run=run_detection)

    # Join any processes
    join_processes(gui_process, detection_process)
# __________________________________________________________________________________________________END of function main


if __name__ == '__main__':
    main()
# ___________________________________________________________________________________________________________EOF of mads
