import numpy as np
import math


class AudioFeatureExtraction:
    ########################################################################################
    #
    # Description: This class is the suite of tools for feature extraction of an audio signal
    #              From an FFT. Not very modular, but any values we can get from audio signals
    #              currently belong to this class.
    #
    # @instance var self.bin_lower (int) - The lower bound of frequencies to consider when making an audio
    #                                      signature.
    #
    # @instance var self.bin_upper (int) - The upper bound of frequencies to consider when making an audio
    #                                      signature.
    #
    # @instance var self.bin_size (int) - Bin size to put frequencies into. For example: If the bin_lower
    #                                     bound is 500, a bin_size of 25 would place all frequencies
    #                                     between 500-525 into bin 1 (counted as one occurrence).
    #                                     525 < frequencies <= 550 into bin 2. 550 < frequencies
    #                                     <=575 into bin 3... and so on.
    #
    # @instance var self.bins (ndarray) - The bin structure defined by self.bin_lower, self.bin_upper,
    #                                     and self.bin_size. For example: self.bins = np.arange(100, 1000, 50)
    #                                     self.bins is the ndarray [100, 150, 200, 250, 300, ..., 900, 950].
    #                                     https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.arange.html
    #
    # @instance var self.number_of_peak_frequencies (int) - Number of peak frequencies to extract from an FFT.
    #                                               Peak frequencies are defined as the frequencies with the highest
    #                                               amplitude. For example: (fake fft) [1, 2, 4, 5, 100, 23, 50, 30] if
    #                                               self.number_of_peak_frequencies is 2. The resulting array is [4, 6]
    #                                               which is stored in self.indices_of_spectral_peaks.
    #
    # @instance var self.binned_indices (ndarray of ints) - The binned values of the spectral peaks that uses self.bins
    #                                                       to define the bin.
    #
    # @instance var mads_rfft (MADSRFFT) - The rfft object. Used here to extract frequency values.
    #
    # @instance var total_energy (float) - The float value of the total energy for the current spectrum of a signal.
    #
    # @instance print_count (int) - The count of how many times this object has been printed in a given run.
    #                               Useful for visualization.
    #
    ########################################################################################

    def __init__(self, bin_lower=79, bin_upper=3078, bin_size=25, number_of_peak_frequencies=20, mads_rfft=None):

        # Signature generation variables using bins
        self.bin_lower = bin_lower
        self.bin_upper = bin_upper
        self.bin_size = bin_size
        self.bins = np.arange(self.bin_lower, self.bin_upper, self.bin_size)
        self.number_of_peak_frequencies = number_of_peak_frequencies
        self.indices_of_spectral_peaks = None
        self.binned_indices = None
        self.mads_rfft = mads_rfft

        # Total energy
        self.total_energy = 0.0

        # Print counter for __str__
        self.print_count = 0

# _______________________________________________________________________________________________________END of __init__

    def get_spectral_peaks(self, spectrum):
        ########################################################################################
        #
        # Description: This method finds the indices of the spectral peaks for a given spectrum.
        #              These indices are returned as frequency values to the caller.
        #
        # @param spectrum (ndarray) - The rfft data.
        #
        # @return (string) - The string representation of the peak frequencies.
        #
        # NOTE: The number of peak frequencies returned is defined by number_of_peak_frequencies
        #       which is init at creation of an AudioFeatureExtraction object.
        #
        # FINAL NOTE: If you want to assure yourself that this works... use a perfect sin wave as input
        #             and change self.number_of_peak_frequencies to equal 1. The output should match
        #             the frequency of the input.
        #
        ########################################################################################

        # Determine the indices of the spectral peaks
        self.indices_of_spectral_peaks = \
            np.argpartition((spectrum), -self.number_of_peak_frequencies)[-self.number_of_peak_frequencies:]

        # Convert values at indices into frequency
        peaks_as_a_frequency = str(self.mads_rfft.freq_values[self.indices_of_spectral_peaks].astype(int))

        return peaks_as_a_frequency

# ____________________________________________________________________________________END of function get_spectral_peaks

    def bin_spectral_peaks(self):
        ########################################################################################
        #
        # Description: This method places the spectral peaks (as frequency) into bins based
        #              on the params defined in self.bins. See class doc for more information.
        #
        # @param n/a
        #
        # @return self.binned_indices (ndarray of ints) - The binned values of the spectral peaks that uses self.bins
        #                                                 to define the bin.
        #
        ########################################################################################
        self.binned_indices = \
            np.digitize(self.mads_rfft.freq_values[self.indices_of_spectral_peaks].astype(int), self.bins)

        return self.binned_indices
# ____________________________________________________________________________________END of function get_spectral_peaks

    def get_total_energy(self, spectrum):
        ########################################################################################
        #
        # Description: The method computes the total energy of a given signal from an rfft.
        #
        # @param spectrum (ndarray) - The rfft data.
        #
        # @return self.total_energy (float) - The total energy of a signal.
        #
        # NOTE: http://blog.prosig.com/2015/01/06/rms-from-time-history-and-fft-spectrum/
        #       See equation 2.
        #
        ########################################################################################

        self.total_energy = math.sqrt(2 * (sum(x * x for x in spectrum)))

        return self.total_energy

    def __str__(self):
        ########################################################################################
        #
        # Description: Print an AudioFeatureExtraction to see the information below. Dunder methods...
        #              For example...
        #              my_extractor = AudioFeatureExtraction()
        #              print(my_extractor)
        #
        #
        # @param n/a
        #
        # @return self.binned_indices (ndarray of ints) - The binned values of the spectral peaks that uses self.bins
        #                                                 to define the bin.
        #
        ########################################################################################

        to_string = '==========================================START-FEATURE====================================' \
                    '================\n' + str(self.number_of_peak_frequencies) + ' highest bins from the RFFT are: \n' \
                    + str(self.indices_of_spectral_peaks) + '\n\n' + str(self.number_of_peak_frequencies) + ' highest bins from the RFFT ' \
                    'represented as a frequency: \n' + str(self.mads_rfft.freq_values[self.indices_of_spectral_peaks].astype(int)) \
                    + '\n\nThe indicies at which the ' + str(self.number_of_peak_frequencies) + ' ' \
                    'highest frequencies live:\n     binLower: ' + str(self.bin_lower) + '\n     binUpper: ' \
                    + str(self.bin_upper) + '\n     bin_size: ' + str(self.bin_size) + '\n     INDICES: ' \
                    + str(self.binned_indices) + '\n' + '==========================================END==============' \
                     '==============================================' + 'PRINT COUNT: ' + str(self.print_count) + '\n'

        # Increase the print count by one
        self.print_count += 1

        return to_string
# ________________________________________________________________________________________________________END of __str__

# ___________________________________________________________________________________________END class MADSFingerPrinter
