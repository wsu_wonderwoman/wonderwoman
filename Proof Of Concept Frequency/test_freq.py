#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of AudioLazy, the signal processing Python package.
# Copyright (C) 2012-2016 Danilo de Jesus da Silva Bellini
#
# AudioLazy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##ARE MATT'S COMMENTS##
"""
Matplotlib animated plot with mic input data.

Call with the API name like ...
  ./animated_plot.py jack
... or without nothing for the default PortAudio API.
"""
from __future__ import division
from audiolazy import sHz, chunks, AudioIO, line, pi, window
from numpy.fft import rfft
import numpy as np
import collections, sys, threading

#my_array = np.array([1894, 1518])

#Creating np array that starts at 500 and goes to 2000 in increments of 50
bins = np.arange(500, 2000, 25)

# AudioLazy init
rate = 44100
###################################
##lazy_misc.py
##return float(rate), 2 * pi / rate
###################################
s, Hz = sHz(rate) 
###################################
##2^12 4096
###################################
length = 2 ** 12
###################################
##Deque... where the deque contains 0.0 4096 times... length of the deque is 4096
##Note deques append to the right and by default pop from the left when full
###################################
data = collections.deque([0.] * length, maxlen=length)
###################################
## For FFT ##come back to me
###################################
wnd = np.array(window.hamming(length))
###################################
##IGNORE FOR NOW
###################################
api = None
###################################
##lazy.io COME BACK TO ME
###################################
##chunks.size = 16##Dont think I need this
###################################
##Note: // is floor division##
##line define in lazy_synth... line takes first arguement as the number
##"partions" the second argument as the starting partition and the last
##arg as the ending position so in this case 4096 elements spaced evenly from
##0 to 2*pi/Hz
##NOTE: Option 4th arg finish=True or False where if true goes to the end
##value otherwise stops before it defaults to false
##Second NOTE: similar to np.linspace
##Uses generator from line function I think we are ignoring the negs here
###################################
freq_values = np.array(line(length, 0, 2 * pi / Hz).take(length // 2 + 1))
# Creates a data updater callback
def update_data():
  with AudioIO(api=api) as rec:
    for el in rec.record(rate=rate):
      data.append(el)
      if update_data.finish:
        break

# Creates the data updater thread
update_data.finish = False
th = threading.Thread(target=update_data)
th.start() # Already start updating data


def update():

    array_data = np.array(data)
    spectrum = np.abs(rfft(array_data * wnd)) / length

    vals = np.argpartition((spectrum), -20)[-20:]

    inds = np.digitize(freq_values[vals].astype(int), bins)

    # Print test
    #print("HIGHEST FREQ:", freq_values[spectrum.argmax()].astype(int))
    #print("VALUES: ", vals)
    #print("FREQ: ", freq_values[vals].astype(int))
    print("INDEX: ", inds)

    # The idea here is we make a range and look for 5 "keys" This will need to be cleaned up for searching a large set
    # if we decide to go this route
    if np.any(inds[:] == 42) and np.any(inds[:] == 37) and np.any(inds[:] == 51) and np.any(inds[:] == 57) \
            and np.any(inds[:] == 11) and np.any(inds[:] == 26):
        while True:
                print('Maybe this is a drone?')

while True:
    update()

