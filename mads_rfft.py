from __future__ import division
from audiolazy import sHz, chunks, AudioIO, line, pi, window
from numpy.fft import rfft
import numpy as np


class MadsRFFT:
    # TODO Comment me

    def __init__(self, length=2**12, Hz=None):

        # Passed values
        self.length = length
        self.Hz = Hz
        self.print_count = 0

        # Spectrum created by the fft
        self.spectrum = None

        # Values the FFT uses
        self.wnd = np.array(window.hamming(self.length))
        self.freq_values = np.array(line(self.length, 0, 2 * pi / self.Hz).take(self.length // 2 + 1))

# _______________________________________________________________________________________________________END of __init__

    def run_rfft(self, data):
        # TODO comment me

        array_data = np.array(data)
        self.spectrum = np.abs(rfft(array_data * self.wnd)) / self.length

        return self.spectrum

# ______________________________________________________________________________________________END of function run_rfft

    def __str__(self):
        # TODO Comment me

        to_string = '==========================================START-RFFT=======================================' \
                    '===============\nThe length of the output or number of frequency bins is: ' \
                    + str(len(self.freq_values)) + '\nThe space between each frequency bin is: ' \
                    + str((self.freq_values[2] - self.freq_values[1])) + ' Hz\nThe spectral peak of the RFFT ' \
                    'frequency (as a whole number) is: ' + str(self.freq_values[self.spectrum.argmax()].astype(int)) \
                    + '\nThe frequency bin at which the spectral peak is located: ' + str(self.spectrum.argmax()) \
                    + '\n==========================================END==============' \
                     '==============================================' + 'PRINT COUNT: ' + str(self.print_count) + '\n'

        self.print_count += 1

        return to_string
# ________________________________________________________________________________________________________END of __str__

# _________________________________________________________________________________________________END of class MadsRFFT
